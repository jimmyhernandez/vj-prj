export default [
  { id: 1, name: 'BMW', price: 12 },
  { id: 2, name: 'Apple', price: 32 },
  { id: 3, name: 'Google', price: 21 },
  { id: 4, name: 'Adobe', price: 121 }
];