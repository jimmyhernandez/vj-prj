import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import { routes } from './routes';


import store from './store/store';

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.filter('currency',(value) => {
  return '$' + value.toLocaleString();
});

Vue.http.options.root = 'https://stocksvuejs.firebaseio.com/';

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
